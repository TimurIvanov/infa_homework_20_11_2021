import socket


def listen(host: str = "127.0.0.1", port: int = 3000):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    s.bind((host, port))
    print(f"Listening at {host}:{port}")

    rooms = [[]]
    users_addr = []
    i = 0
    while True:
        msg, addr = s.recvfrom(4096)

        if addr not in rooms[i]:
            if addr not in users_addr:
                users_addr.append(addr)
                if len(rooms[i]) == 2:
                    rooms.append([])
                    i += 1
                rooms[i].append(addr)

        if not msg:
            continue

        client_id = addr[1]
        if msg.decode('ascii') == '__join':
            print(f"Client {client_id} joined room {i+1}")
            continue

        msg = f'client{client_id}: {msg.decode("ascii")}'

        for room in range(len(rooms)):
            if addr not in rooms[room]:
                continue

            for member in rooms[room]:
                if member == addr:
                    continue

                s.sendto(msg.encode('ascii'), member)


if __name__ == "__main__":
    listen()